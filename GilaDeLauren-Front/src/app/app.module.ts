import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Angular Material
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule, MatCheckboxModule, MatCardModule } from '@angular/material';

// Bulma (And Font Awesome)
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ScrollingRibbonComponent } from './scrolling-ribbon/scrolling-ribbon.component';
import { FanMenuComponent } from './fan-menu/fan-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ScrollingRibbonComponent,
    FanMenuComponent
  ],
  imports: [
    BrowserModule,
    
    // Angular Material
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatCardModule,

    // Bulma (And Font Awesome)
    FontAwesomeModule,
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
