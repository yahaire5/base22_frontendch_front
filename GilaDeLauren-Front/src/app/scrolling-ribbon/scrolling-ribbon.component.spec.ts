import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollingRibbonComponent } from './scrolling-ribbon.component';

describe('ScrollingRibbonComponent', () => {
  let component: ScrollingRibbonComponent;
  let fixture: ComponentFixture<ScrollingRibbonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrollingRibbonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollingRibbonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
