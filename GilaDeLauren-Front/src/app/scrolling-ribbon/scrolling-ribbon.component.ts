import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scrolling-ribbon',
  templateUrl: './scrolling-ribbon.component.html',
  styleUrls: ['./scrolling-ribbon.component.scss']
})
export class ScrollingRibbonComponent implements OnInit {
  itemData = [];

  @Input() title: string;
  @Input() icon: string;
  @Input() serviceUrl: string;
  @Input() colorSet: string; // blue; green

  constructor() { 
  }

  ngOnInit() {
    this.getItemData();
  }

  /**
   * Gets the item data from the database
   */
  async getItemData() {
    // TODO: insert service call here
    if (this.serviceUrl == 'get_cooking') {
      this.itemData = await [
        {
          img: 'assets/stock-images/img-deviled-eggs.png',
          title: `Bar Americain's Pickled Shrimp Deviled Eggs with Cornichon Remoulade`,
        },
        {
          img: 'assets/stock-images/img-roulade-cover.png',
          title: `Turkey Roulade with "Stovetop" Stuffing`,
        },
        {
          img: 'assets/stock-images/img-fresh-tomato-sauce-cover.png',
          title: `Pasta with Fresh Tomato Sauce`,
        },
      ];
    }
    else if (this.serviceUrl == 'restaurants') {
      this.itemData = await [
        {
          img: `assets/stock-images/img-restaurante-1.png`,
          title: `DELI BAR`,
          description: '156 Lafayette Street\nNew Tork, NY 10012\n212 677 6400\ndelibar.com',
        },
        {
          img: `assets/stock-images/img-restaurant-2.png`,
          title: `BAR NEW YORK`,
          description: '756 W. 52nd Street\nNew Tork, NY 10019\n214 265 8900\nbar-bew-york.com',
        },
        {
          img: `assets/stock-images/img-restaurant-3.png`,
          title: `BAR TOKIO SUN`,
          description: '11 Mohegan Sun Boulevard\nUncasville, CT 06382\n756 865 3529\ntokiosun.com',
        },
      ];
    }
  }
}
