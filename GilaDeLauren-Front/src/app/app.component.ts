import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Gila De Lauren';

  fanMenuButtons = [
    {
      icon: 'video',
      title: 'WATCH'
    },
    {
      icon: 'file-alt',
      title: 'READ'
    },
    {
      icon: 'envelope',
      title: 'NEWSLETTER'
    },
    {
      icon: 'shopping-cart',
      title: 'SHOP'
    },
    {
      icon: 'search',
      title: 'FOLLOW HER'
    },
  ];

  public constructor(private titleService: Title ) {
    this.setTitle();
  }

  public setTitle() {
    this.titleService.setTitle(this.title);
  }
}
