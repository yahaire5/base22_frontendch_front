import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fan-menu',
  templateUrl: './fan-menu.component.html',
  styleUrls: ['./fan-menu.component.scss']
})
export class FanMenuComponent implements OnInit {

  @Input() buttons: [];

  constructor() { 
  }

  ngOnInit() {
    if (!this.buttons) {
      this.buttons = [];
      console.error("No buttons provided for fan-menu");
    }

    console.log(`there are ${this.buttons.length} buttons in here!`);
    console.log(this.buttons);
  }

}
